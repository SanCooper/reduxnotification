import {Text, StyleSheet, View, Alert} from 'react-native';
import React, {Component} from 'react';
import Navigation from './src/Navigation';
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Persistor, Store} from './src/Redux/store';

// foreground
messaging().onMessage(async remoteMessage => {
  Store.dispatch({
    type: 'ADD-MESSAGE',
    payload: 
      {
        title: remoteMessage.notification.title,
        body: remoteMessage.notification.body,
        img: remoteMessage.notification.android.imageUrl,
        sentTime: remoteMessage.sentTime,
        isRead: 0,
      },
    
  });
});

// background
messaging().setBackgroundMessageHandler(async remoteMessage => {
  Store.dispatch({
    type: 'ADD-MESSAGE',
    payload: 
      {
        title: remoteMessage.notification.title,
        body: remoteMessage.notification.body,
        img: remoteMessage.notification.android.imageUrl,
        sentTime: remoteMessage.sentTime,
        isRead: 0,
      },
    
  });
});

// const setInboxData = async remoteMessage => {
//   try {
//     let getInboxData = await AsyncStorageLib.getItem('newInboxData');
//     getInboxData = JSON.parse(getInboxData);
//     console.log(remoteMessage);
//     if (!getInboxData) {
//       AsyncStorageLib.setItem(
//         'newInboxData',
//         JSON.stringify([
//           {
//             title: remoteMessage.notification.title,
//             body: remoteMessage.notification.body,
//             img: remoteMessage.notification.android.imageUrl,
//             sentTime: remoteMessage.sentTime,
//             data: remoteMessage.data,
//             isRead: 0,
//           },
//         ]),
//       );
//     } else {
//       const arrayData = [
//         ...[
//           {
//             title: remoteMessage.notification.title,
//             body: remoteMessage.notification.body,
//             img: remoteMessage.notification.android.imageUrl,
//             sentTime: remoteMessage.sentTime,
//             data: remoteMessage.data,
//             isRead: 0,
//           },
//         ],
//         ...getInboxData,
//       ].slice(0, 20);
//       AsyncStorageLib.setItem('newInboxData', JSON.stringify(arrayData));
//     }
//   } catch (error) {
//     console.log(error);
//   }
// };

const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
