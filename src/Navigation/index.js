import {Text, StyleSheet, View} from 'react-native';
import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import Inbox from '../Screens/Inbox';
import DetailMsg from '../Screens/DetailMsg';

const Stack = createNativeStackNavigator();
function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Inbox" component={Inbox} options={{headerShown: false}}/>
        <Stack.Screen name="Detail Message" component={DetailMsg} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default index;
