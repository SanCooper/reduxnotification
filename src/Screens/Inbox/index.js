import {
    Text,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
  } from 'react-native';
  import React, {Component} from 'react';
  import AsyncStorageLib from '@react-native-async-storage/async-storage';
  import AntDesign from 'react-native-vector-icons/AntDesign';
  import messaging from '@react-native-firebase/messaging';
  import {convertDateTime, convertTime} from '../../Componentss/Utils/moment';
  import {connect} from 'react-redux';
  
  class index extends Component {
    constructor() {
      super();
      this.state = {
        data: [],
      };
    }
  
    componentDidMount() {
      // this._checkToken();
    }
  
    _checkToken = async () => {
      const fcmToken = await messaging().getToken();
      if (fcmToken) {
         console.log(fcmToken);
      } 
     }

     _deleteInbox = () => {
      Alert.alert(
        'Hapus semua?',
        'Setelah Anda menghapus semua pesan, Anda tidak dapat membatalkannya.',
        [
          {
            text: 'HAPUS',
            onPress: () => this._delete(),
          },
          {text: 'Batal', onPress: () => console.log('Batal Pressed')},
        ],
      );
    };

     _delete = () => {
      this.props.delete();
    };
  
    render() {
      // const {inbox} = this.state;
      const {navigation, inbox} = this.props;
      console.log(inbox);
      return (
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={styles.header}>
            <View>
              <Text style={{fontSize: 25, fontWeight: 'bold', color: 'black'}}>
                Pesan
              </Text>
            </View>
            <View>
              <TouchableOpacity onPress={() => this._deleteInbox()}>
                <AntDesign name="delete" size={30} style={{color: 'black'}} />
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView>
            {inbox.length > 0 ? (
              inbox.map((value, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => navigation.navigate('Detail Message', value)}
                    style={styles.inboxCard}>
                    <View>
                      <Text style={styles.inboxTitle}>{value.title}</Text>
                      <Text style={styles.inboxBody}>
                        {value.body.substr(0,100)}
                      </Text>
                      <Text style={styles.inboxDate}>
                        {convertDateTime(new Date(value.sentTime))}
                      </Text>
                    </View>
                    <View>
                      <AntDesign name="right" size={30} />
                    </View>
                  </TouchableOpacity>
                );
              })
            ) : (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingTop: '50%',
                }}>
                <Text>Tidak ada pesan.</Text>
              </View>
            )}
          </ScrollView>
        </View>
      );
    }
  }
  
  const mapStateToProps = state => {
    return {
      inbox: state.inbox
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      delete: () => {
        dispatch({
          type: 'DELETE-MESSAGE',
        });
      },
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(index);
  
  const styles = StyleSheet.create({
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderBottomWidth: 1,
      padding: 10,
    },
    inboxCard: {
      borderBottomWidth: 1,
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: 20,
      // marginRight: 20,
      // marginLeft:20,
      paddingRight: 20,
      paddingVertical: 12,
      justifyContent: 'space-between',
      backgroundColor: 'white',
      borderBottomColor: 'grey',
    },
    inboxTitle: {
      fontSize: 19,
      fontWeight: 'bold',
      color: 'black',
    },
    inboxBody: {
      fontSize: 15,
      paddingTop: 5,
      color: 'black',
    },
    inboxDate: {
      fontSize: 13,
      paddingTop: 30,
    },
  });
  