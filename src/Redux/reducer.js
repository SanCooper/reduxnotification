const initialState = {
    inbox: [],
  };
  
  const reducer = (state = initialState, action) => {
      console.log(state, action)
    switch (action.type) {
      case 'ADD-MESSAGE':
        return {
          ...state,
          inbox: [action.payload, ...state.inbox]
        };
        case 'DELETE-MESSAGE':
        return {
          ...state,
          inbox: [],
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  